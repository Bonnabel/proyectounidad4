'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AutoSchema = Schema({
	nombre: String,
	marca: String,
	modelo: String,
	costo: {type: Number, default: 0}
});

module.exports = mongoose.model('Auto', AutoSchema);