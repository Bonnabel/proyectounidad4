'use strict'

const Auto = require('../models/autos');

function getAuto(req, res){
  let autoId = req.params.autoId;
  
  Auto.findById(autoId, (err, auto) => {
    if (err) return res.status(500).send({message: `Error al realizar la peticion ${err}`});
    if (!auto) return res.status(404).send({message: `El auto no existe`});
  
    res.status(200).send({auto}); 
  });
}

function getAutos(req, res){
  Auto.find({}, (err, autos) => {
    if (err) return res.status(500).send({message: `Error al realizar la peticion ${err}`});
    if (!autos) return res.status(404).send({message: `El auto no existe`});

    res.send(200, {autos});
  });
}

function saveAuto(req, res){
  console.log('POST /api/auto');
  console.log(req.body);

  let auto = new Auto();
  auto.nombre = req.body.nombre;
  auto.marca = req.body.marca;
  auto.modelo = req.body.modelo;
  auto.costo = req.body.costo;
  

  auto.save((err, autoStored) => {
    if (err) res.status(500).send({message: `Error al salvar en la base de datos: ${err}`});

    res.status(200).send({auto: autoStored});
  });
}

function updateAuto(req, res){
  let autoId = req.params.autoId;
  let update = req.body;

  Auto.findByIdAndUpdate(autoId, update, (err, autoUpdated) => {
    if (err) res.status(500).send({message: `Error al actualizar el auto: ${err}`});

    res.status(200).send({auto: autoUpdated});
  })
}

function deleteAuto(req, res){
  let autoId = req.params.autoId;
  
  Auto.findById(autoId, (err, auto) => {
    if (err) res.status(500).send({message: `Error al borrar el auto: ${err}`});
  
    auto.remove(err => {
      if (err) res.status(500).send({message: `Error al borrar el auto: ${err}`});
      res.status(200).send({message: 'El auto ha sido eliminado'});
    })
  })
}

module.exports = {
  getAuto,
  getAutos,
  saveAuto,
  updateAuto,
  deleteAuto
}