'use strict'

const express = require('express');
const autoCtrl = require('../controllers/autos');
const userCtrl = require('../controllers/user');
const auth = require('../middlewares/auth');
const api = express.Router();

//Routes para autos
api.get('/auto', autoCtrl.getAutos);
api.get('/auto/:autoId', auth, autoCtrl.getAuto);

api.post('/auto', autoCtrl.saveAuto);
api.put('/auto/:autoId', autoCtrl.updateAuto);
api.delete('/auto/:autoId', autoCtrl.deleteAuto);


//Routes para autenticacion
api.post('/signup', userCtrl.signUp);
api.post('/signin', userCtrl.signIn);
api.get('/private', auth, function(req, res){
  res.status(200).send({message: 'Tienes acceso'});
});

module.exports = api;